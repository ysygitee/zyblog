package com.zyd.blog.business.enums;

/**
 * @author qing.huan
 * @version 1.0
 * @website https://www.zhyd.me
 * @date 2018/4/16 16:26
 * @since 1.0
 */
public enum FileUploadType {
    COMMON("ZyBLOG/"),
    QRCODE("ZyBLOG/qrcode/"),
    SIMPLE("ZyBLOG/article/"),
    COVER_IMAGE("ZyBLOG/cover/");

    private String path;

    FileUploadType(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
